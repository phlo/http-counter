package http_counter

import (
	"fmt"
	"net/http"
)

var n int

func (c *preciseCounter) HttpHandler(w http.ResponseWriter, r *http.Request) {
	lock.Lock()
	defer lock.Unlock()
	n++
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	c.Add()
	err := c.Save()
	if err != nil {
		fmt.Fprintln(w, "Error saving")
	}
	fmt.Fprintln(w, c.Count())
	fmt.Println(n, " ", c.Count())
}
