package main

import (
	"fmt"
	"gitlab.com/phlo/http-counter"
	"log"
	"net/http"
	"time"
)

func main() {
	counter, err := http_counter.NewPreciseCounter(5*time.Second, http_counter.IN_TMP)
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println("counter httpd listening on 8080...")

	http.HandleFunc("/", counter.HttpHandler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
