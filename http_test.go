package http_counter

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"
)

func TestPreciseCounter_HttpHandler(t *testing.T) {
	c, err := NewPreciseCounter(500*time.Millisecond, IN_TMP)
	if err != nil {
		t.Fatal(err)
	}
	handler := http.HandlerFunc(c.HttpHandler)

	request, _ := http.NewRequest(http.MethodGet, "/404", nil)
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, request)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusNotFound)
	}

	request.URL.Path = "/"
	rr = httptest.NewRecorder()
	handler.ServeHTTP(rr, request)

	got := strings.TrimSpace(rr.Body.String())
	if got != "1" {
		t.Errorf("got %s, want %s", rr.Body.String(), "1")
	}

	c.Window = 1 * time.Second
	for i := 0; i < 300; i++ {
		handler.ServeHTTP(rr, request)
	}
	count := c.Count()
	if count != 301 {
		t.Errorf("counted %d, expected %d", count, 301)
	}
}

func BenchmarkPreciseCounter_HttpHandler(b *testing.B) {

}
