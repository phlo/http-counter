package http_counter

import (
	"testing"
	"time"
)

func TestTimestampConversions(t *testing.T) {
	first := time.Now()
	ts := timeToTimestamp(first) // 1516963045956067053
	second, err := timestampToTime(ts)
	if err != nil {
		t.Fatal("Timestamp parse fail")
	}
	if second.Nanosecond() != first.Nanosecond() {
		t.Fatal("Timestamp conversion is flawed")
	}
}

func TestPreciseCounter_Count(t *testing.T) {
	c := &preciseCounter{Window: 100 * time.Millisecond}
	c.Add()
	c.Add()
	if c.Count() != 2 {
		t.Fatalf("Counting doesn't work (count returned %d instead of %d)", c.Count(), 2)
	}
	time.Sleep(50 * time.Millisecond)
	c.Add()
	c.Add()
	time.Sleep(80 * time.Millisecond)
	c.Add()
	if c.Count() != 3 {
		t.Fatalf(
			"Counting doesn't work well with a window interval. Counting returned %d instead of %d",
			c.Count(),
			3,
		)
	}
}

func TestPreciseCounter_Cleanup(t *testing.T) {
	c, _ := NewPreciseCounter(100*time.Millisecond, IN_TMP)
	c.Add()
	time.Sleep(130 * time.Millisecond)
	c.Add()
	c.Cleanup()
	if len(c.events) != 1 {
		t.Fatal("Problem with trimming old events")
	}
}
