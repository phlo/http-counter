package http_counter

import (
	"bytes"
	"encoding/json"
	"io"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	IN_TMP       = "IN_TMP"
	DEFAULT_PATH = "httpd_counter"
)

type StateHandler interface {
	// todo these should receive interface{} instead of the default timeline
	// , so that the handler could work with any data type
	// (eg. different kind of events data structure)
	Save([]time.Time, TimelineMarshaler) error
	Load(*[]time.Time, TimelineUnmarshaler) error
}

type TimelineMarshaler func([]time.Time) (io.Reader, error)

type TimelineUnmarshaler func(io.Reader, *[]time.Time) error

// A StateHandler implementation
type state struct {
	Path string
}

func NewStateHandler(path string) *state {
	if path == IN_TMP {
		path = os.TempDir() + "/" + DEFAULT_PATH
	}
	return &state{Path: path}
}

// human readable, could be more efficient
func Marshal(timeLine []time.Time) (io.Reader, error) {
	b, err := json.MarshalIndent(timeLine, "", "\t")
	if err != nil {
		return nil, err
	}
	return bytes.NewReader(b), nil
}

func Unmarshal(r io.Reader, timeLine *[]time.Time) error {
	return json.NewDecoder(r).Decode(timeLine)
}

// ideally I would have used one channel for IO and others for publishing to it, but this will do
var lock sync.Mutex

func (s *state) Save(timeline []time.Time, marshaler TimelineMarshaler) error {
	f, err := os.Create(s.Path)
	if err != nil {
		return err
	}
	defer f.Close()

	r, err := marshaler(timeline)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, r)
	return err
}

// Loads previously saved data
func (s *state) Load(timeline *[]time.Time, unmarshaler TimelineUnmarshaler) error {
	lock.Lock()
	defer lock.Unlock()
	f, err := os.Open(s.Path)
	if err != nil {
		return err
	}
	defer f.Close()
	return unmarshaler(f, timeline)
}

// todo change in a future better Marshal() where there is 1 nanotimestamp per line
func timeToTimestamp(t time.Time) string {
	return strconv.FormatInt(t.UnixNano(), 10)
}

// todo use in a future better Unmarshal()
func timestampToTime(timestamp string) (*time.Time, error) {
	ts, err := strconv.ParseInt(timestamp, 10, 64)
	if err != nil {
		return nil, err
	}
	t := time.Unix(0, ts)
	return &t, nil
}
