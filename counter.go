package http_counter

import (
	"os"
	"time"
)

// todo "precise" because I'm also thinking of an opportunistic implementation
// which logs the number of events per each really small interval
// composing the desired window back in time
// (eg. number of events per each half of a second composing the 60s)
type preciseCounter struct {
	Window       time.Duration
	StateHandler StateHandler
	events       []time.Time
}

func (c *preciseCounter) Count() int {
	c.Cleanup()
	return len(c.events)
}

// Appends on the first position
func (c *preciseCounter) Add() {
	c.events = append([]time.Time{time.Now()}, c.events...)
}

// Discards everything that is too old
func (c *preciseCounter) Cleanup() {
	for i, t := range c.events {
		if time.Since(t) > c.Window {
			c.events = c.events[:i]
			return
		}
	}
}

func (c *preciseCounter) Save() error {
	return c.StateHandler.Save(c.events, Marshal)
}

func (c *preciseCounter) Load() error {
	return c.StateHandler.Load(&c.events, Unmarshal)
}

func NewPreciseCounter(timeWindow time.Duration, logPath string) (c *preciseCounter, err error) {
	stateHandler := &state{Path: logPath}

	// pseudo easter egg yay
	if logPath == IN_TMP {
		stateHandler.Path = os.TempDir() + "/" + "httpd-counter-unit-test"
	}

	c = &preciseCounter{
		Window:       timeWindow,
		StateHandler: stateHandler,
	}

	if err := c.StateHandler.Load(&c.events, Unmarshal); err != nil && !os.IsNotExist(err) {
		return nil, err
	}

	c.Cleanup()

	return
}
